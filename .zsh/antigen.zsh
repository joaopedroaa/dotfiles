source ~/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh
antigen bundle sobolevn/wakatime-zsh-plugin
antigen apply
