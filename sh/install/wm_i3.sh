echo "========================================== I3 (2) ====================="
yay -S i3-gaps autotiling-git --noconfirm

echo "========================================== Lock (1) ====================="
yay -S i3lock --noconfirm

echo "========================================== Bar (3) ====================="
yay -S i3blocks py3status polybar --noconfirm

