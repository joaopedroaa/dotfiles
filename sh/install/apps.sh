echo "========================================== Torrent (2) ====================="
yay -S qbittorrent stremio --noconfirm

echo "========================================== Apps (3) ====================="
yay -S discord zoom bitwarden-bin --noconfirm

echo "========================================== Browsers (3) ====================="
yay -S google-chrome tor-browser firefox-developer-edition --noconfirm

echo "========================================== Phone (1) ====================="
yay -S kdeconnect --noconfirm

echo "========================================== Media / Image (1) ====================="
yay -S krita --noconfirm

echo "========================================== Media / Video (2) ====================="
yay -S vlc obs-studio --noconfirm

echo "========================================== Hardware / Boot (3) ====================="
yay -S os-prober grub-customizer woeusb --noconfirm

echo "========================================== Hardware / Cpu (3) ====================="
yay -S cpu-x-git s-tui stress --noconfirm

echo "========================================== Hardware / Gpu (1) ====================="
yay -S gwe --noconfirm

echo "========================================== Hardware / Hd (2) ====================="
yay -S gparted-git gsmartcontrol --noconfirm

echo "========================================== Driver / Gpu (4) ====================="
yay -S nvidia nvidia-utils nvidia-settings mesa-demos --noconfirm

echo "========================================== Console (3) ====================="
yay -S gotop-bin htop xclip --noconfirm

echo "========================================== Console / Fun (4) ====================="
yay -S cava cbonsai neofetch lolcat --noconfirm

echo "========================================== Console / Zsh (2) ====================="
yay -S zsh oh-my-zsh-git --noconfirm

echo "========================================== Console / Zsh / Config (2) ====================="
yay -S antigen-git spaceship-prompt-git --noconfirm

echo "========================================== Spotify (2) ====================="
yay -S spotify-snap spotify-adblock-linux --noconfirm

echo "========================================== Spotify / Spicetify (2) ====================="
yay -S spicetify-cli spicetify-themes-git --noconfirm

echo "========================================== Fonts / Nerd (1) ====================="
yay -S nerd-fonts-complete --noconfirm

echo "========================================== Fonts / Ttf (9) ====================="
yay -S ttf-fira-code ttf-ibm-plex-mono-git ttf-liberation ttf-roboto ttf-dejavu ttf-droid ttf-inconsolata ttf-liberation ttf-unifont --noconfirm

echo "========================================== Fonts / Misc (1) ====================="
yay -S awesome-terminal-fonts --noconfirm

echo "========================================== Fix-bugs (3) ====================="
yay -S gnome-keyring sshfs ntfs-3g --noconfirm

