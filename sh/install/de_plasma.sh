echo "========================================== Filemanager (2) ====================="
yay -S dolphin dolphin-plugins --noconfirm

echo "========================================== Desktop (2) ====================="
yay -S plank superpaper --noconfirm

echo "========================================== Unpackfiles (1) ====================="
yay -S ark --noconfirm

echo "========================================== Console (2) ====================="
yay -S konsole yakuake --noconfirm

echo "========================================== Kvantum (2) ====================="
yay -S kvantum-qt5-git kvantum-theme-layan-git --noconfirm

echo "========================================== Pdf (1) ====================="
yay -S okular --noconfirm

echo "========================================== Media / Image (1) ====================="
yay -S gwenview --noconfirm

