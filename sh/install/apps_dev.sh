echo "========================================== Cli (4) ====================="
yay -S vsce exercism wakatime openssh --noconfirm

echo "========================================== Vm (2) ====================="
yay -S docker virtualbox-ext-oracle --noconfirm

echo "========================================== Api (2) ====================="
yay -S insomnia-bin postman-bin --noconfirm

echo "========================================== Editors (2) ====================="
yay -S visual-studio-code-insiders-bin android-studio --noconfirm

echo "========================================== Editors / Emacs (4) ====================="
yay -S emacs emacs-doom-themes-git fd ripgrep --noconfirm

echo "========================================== Lang / Js (2) ====================="
yay -S nvm-git yarn --noconfirm

echo "========================================== Lang / Java (2) ====================="
yay -S java-8-jdk glassfish5 --noconfirm

echo "========================================== Lang / Php (5) ====================="
yay -S apache mysql php php-apache phpmyadmin --noconfirm

echo "========================================== Lang / Python (2) ====================="
yay -S python autopep8 --noconfirm

echo "========================================== Lang / Elixir (2) ====================="
yay -S elixir inotify-tools --noconfirm

echo "========================================== Lang / Haskell (3) ====================="
yay -S ghc cabal-install-bin stack --noconfirm

